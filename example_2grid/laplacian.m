function A = laplacian( N, h, bc )

a = 1/h^2;
b = -2/h^2;
c = 1/h^2;

if (strcmp(bc(1),'periodic') && strcmp(bc(2),'periodic'))
    
    n = N;    
    A = zeros(n,n);
    for i = 1:n
        if (i == 1)
            if (strcmp(bc(1),'periodic'))
                A(i,n) = a;
            end
        else
            A(i,i-1) = a;
        end
        A(i,i) = b;
        if (i == n)
            if (strcmp(bc(2),'periodic'))
                A(i,1) = c;
            end
        else
            A(i,i+1) = c;
        end
    end
    
elseif (strcmp(bc(1),'dirichlet') && strcmp(bc(2),'dirichlet'))
    
    n = N+1;
    A = zeros(n,n);
    A(1,1) = 1.0;
    for i = 2:n-1
        A(i,i-1) = a;
        A(i,i) = b;
        A(i,i+1) = c;
    end
    A(n,n) = 1.0;
    
end

end

