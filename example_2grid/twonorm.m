function a = twonorm(v)

if (size(v,1) == 1)
    v = v';
end

n = size(v,1);

sum = 0.0;
for i = 1:n
    sum = sum + v(i)*v(i);
end

a = sqrt(sum/n);

end

