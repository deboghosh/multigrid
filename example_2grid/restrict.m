function u = restrict(v,bc)

if (strcmp(bc(1),'periodic') && strcmp(bc(2),'periodic'))
    
    if (mod(size(v,1),2) ~= 0)
        fprintf('Error: size of input vector not even - %d\n',size(v,1));
        exit;
    end
    n = size(v,1)/2;
    u = zeros(n,1);
    for i=1:n
        if (i < n)
            u(i) = 0.25*v(2*i-1) + 0.5*v(2*i) + 0.25*v(2*i+1);
        else
            u(i) = 0.25*v(2*i-1) + 0.5*v(2*i) + 0.25*boundary(v,'right',bc);
        end
    end
    
elseif (strcmp(bc(1),'dirichlet') && strcmp(bc(2),'dirichlet'))
    
    if (mod(size(v,1),2) ~= 1)
        fprintf('Error: size of input vector not odd - %d\n',size(v,1));
        exit;
    end
    n = (size(v,1)+1)/2;
    u = zeros(n,1);
    u(1) = v(1);
    for i=2:n-1
        j = (i-1)*2 + 1;
        u(i) = 0.25*v(j-1) + 0.5*v(j) + 0.25*v(j+1);
    end
    u(n) = v(size(v,1));
    
end

end

