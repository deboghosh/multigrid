function v = normalize(u)

n = size(u,1);
v = zeros(n,1);

sum = 0;
for i = 1:n
    sum = sum + u(i);
end
avg = sum/n;

for i = 1:n
    v(i) = u(i) - avg;
end

end

