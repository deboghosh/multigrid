function y = jacobi(A, b, x)

n = size(x,1);
y = zeros(n,1);

Ax = A*x;


omega = 0.9;

for i = 1:n
    y(i) = x(i) + omega * (b(i) - Ax(i)) / A(i,i);
end

end

