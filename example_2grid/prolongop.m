function P = prolongop(N, bc)

if (strcmp(bc(1),'periodic') && strcmp(bc(2),'periodic'))
    
    n = N;
    m = 2*n;
    P = zeros(m,n);
    for i = 1:m
        if (mod(i,2) == 0)
            P(i,i/2) = 1.0;
        else
            if (i == 1)
                P(i,n) = 0.5;
                P(i,1) = 0.5;
            else
                j = (i-1)/2;
                P(i,j) = 0.5;
                P(i,j+1) = 0.5;
            end
        end
    end
    
elseif (strcmp(bc(1),'dirichlet') && strcmp(bc(2),'dirichlet'))
    
    n = N+1;
    m = 2*n - 1;
    P = zeros(m,n);
    P(1,1) = 1.0;
    for i = 2:(m-1)
        if (mod(i,2) == 1)
            j = (i-1)/2+1;
            P(i,j) = 1;
        else
            j = i/2;
            P(i,j) = 0.5;
            P(i,j+1) = 0.5;
        end
    end
    P(m,n) = 1.0;
    
end

end

