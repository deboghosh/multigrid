clear;
close all;

%% basic parameters
Npts = [20; 40; 80; 160];
% bc = {'periodic','periodic'};
bc = {'dirichlet','dirichlet'};
L = 1.0;

%% verbosity
verbose = 0;

%% basic solver type
stype = 'gauss_seidel';

%% iteration parameters
maxits = 100000;
atol = 1e-10;
rtol = 1e-6;

%% multigrid parameters
presmoothing_its = 0;
postsmoothing_its = 5;

nits = zeros(size(Npts,1),1);
nits_mg = zeros(size(Npts,1),1);
cost = zeros(size(Npts,1),1);
cost_mg = zeros(size(Npts,1),1);
wctime = zeros(size(Npts,1),1);
wctime_mg = zeros(size(Npts,1),1);
for i = 1:size(Npts,1)
    
    N = Npts(i);
    
    %% define grid
    h = L/N;
    if ((strcmp(bc(1),'periodic')) && (strcmp(bc(2),'periodic')))
        x = (0:h:(L-h))';
    elseif ((strcmp(bc(1),'dirichlet')) && (strcmp(bc(2),'dirichlet')))
        x = (0:h:L)';
    end
    
    %% get operators
    P = prolongop(N/2, bc);
    R = restrictop(N, bc);
    A = laplacian(N, h, bc);
    
    %% minimum and maximum wavenumber
    kmin = 1;
    kint = 1;
    kmax = 1;
    
    %% define RHS
    b = zeros(size(x,1),size(x,2));
    for k = kmin:kint:kmax
        b = b - (2*pi*k)^2*cos(2*pi*k*x);
    end
    
%     figure(1);
%     plot(x,b,':b','LineWidth',2);
%     hold on;
    
    %% exact solution
    uex = zeros(size(x,1),size(x,2));
    for k = kmin:kint:kmax
        uex = uex + cos(2*pi*k*x);
    end
    figure(i);
    plot(x,uex,'-k','LineWidth',2);
    hold on;
    
    if ((strcmp(bc(1),'dirichlet')) && (strcmp(bc(2),'dirichlet')))
        b(1) = uex(1);
        b(max(size(b))) = uex(max(size(uex)));
    end
    
    %% iterative solution
    fprintf('Basic: solving for N=%d... ', N);
    if (verbose > 0)
        fprintf('\n');
    end
    u0 = zeros(size(x,1),size(x,2));
    tic;
    [u,nits(i)] = solve(A,b,u0,maxits,atol,rtol,stype, verbose,'Basic');
    wctime(i) = toc;
    cost(i) = nits(i);
    fprintf('%d iterations, cost %f.\n',nits(i), cost(i));
    figure(i);
    plot(x,u,'ro','LineWidth',2);
    hold on;
    
    %% two-grid solution
    u_f = zeros(size(x,1),size(x,2));
    if ((strcmp(bc(1),'dirichlet')) && (strcmp(bc(2),'dirichlet')))
        u_f(1) = uex(1);
        u_f(N+1) = uex(N+1);
    end
    fprintf('TwoGrid: solving for N=%d... ', N);
    if (verbose > 0)
        fprintf('\n');
    end
    nits_coarse = 0;
    nits_fine = 0;
    tic;
    for n = 1:maxits
        
        errnorm = twonorm(residual(A,u_f,b));
%         fprintf('  iter = %5d, norm = %1.4e\n', n, errnorm);
        
        if (n == 1)
            norm0 = errnorm;
        end
        
        if (errnorm < atol)
            break;
        end
        if (errnorm/norm0 < rtol)
            break;
        end
        
        % pre-smoothing
        [u_1,n_f] = solve(A,b,u_f,presmoothing_its,atol,rtol,stype, ...
                          verbose, 'TwoGrid (presmoothing)');
        nits_fine = nits_fine + n_f;
        
        r_f = b - A*u_1;
        r_c = R*r_f;
        A_c = R*A*P;
        [v_c,n_c] = solve(A_c,r_c,zeros(size(r_c)),maxits,0.01*errnorm,0.01,stype, ...
                          verbose, 'TwoGrid (coarse)');
%         v_c = A_c\r_c;
%         n_c = 0;
        nits_coarse = nits_coarse + n_c;
        u_2 = u_1 + P*v_c;
        
        % post-smoothing
        [u_f,n_f] = solve(A,b,u_2,postsmoothing_its,atol,rtol,stype, ...
                          verbose, 'TwoGrid (postsmoothing)');
        nits_fine = nits_fine + n_f;
                
    end
    wctime_mg(i) = toc;
    nits_mg(i) = n;
    cost_mg(i) = nits_fine + 0.5*nits_coarse;
    fprintf('%d coarse iterations, %d fine iterations, cost %f.\n', ...
        nits_coarse, nits_fine, cost_mg(i));
    figure(i);
    plot(x,u_f,'bx','LineWidth',2);
    hold off;
    axis tight;
    
end
figidx = i+1;

figure(figidx);
subplot(1,2,1);
loglog(Npts, nits, '-bo', 'LineWidth',2);
hold on;
loglog(Npts, nits_mg, '-ro', 'LineWidth',2);
hold off;
axis tight;
subplot(1,2,2);
loglog(Npts, cost, '-bo', 'LineWidth',2);
hold on;
loglog(Npts, cost_mg, '-ro', 'LineWidth',2);
hold off;
axis tight;
figidx = figidx+1;


