function R = restrictop(N, bc)

if (strcmp(bc(1),'periodic') && strcmp(bc(2),'periodic'))
    
    n = N;
    if (mod(n,2) ~= 0)
        fprintf('Error: n must be even. n is %d\n',n);
        exit;
    end
    m = n/2;
    R = zeros(m,n);
    for i = 1:m
        j = 2*i;
        R(i,j-1) = 0.25;
        R(i,j) = 0.5; 
        if (i == m)
            R(i,1) = 0.25;
        else
            R(i,j+1) = 0.25;
        end
    end
    
elseif (strcmp(bc(1),'dirichlet') && strcmp(bc(2),'dirichlet'))
    
    n = N+1;
    if (mod(n,2) ~= 1)
        fprintf('Error: n must be odd. n is %d\n',n);
        exit;
    end
    m = (n+1)/2;
    R = zeros(m,n);
    R(1,1) = 1.0;
    for i = 2:(m-1)
        j = (i-1)*2 + 1;
        R(i,j-1) = 0.25;
        R(i,j) = 0.5;
        R(i,j+1) = 0.25;
    end
    R(m,n) = 1.0;
    
end


end

