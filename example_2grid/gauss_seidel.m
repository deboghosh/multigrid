function y = gauss_seidel(A, b, x)

n = size(x,1);
y = x;

for i = 1:n
    term = 0;
    for j = 1:n
        if (j ~= i)
            term = term + A(i,j)*y(j);
        end
    end
    y(i) = (b(i) - term) / A(i,i);
end

end

