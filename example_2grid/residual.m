function r = residual( A, u, b )

r = u - A\b;

end

