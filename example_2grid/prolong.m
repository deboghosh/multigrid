function u = prolong(v,bc)

if (strcmp(bc(1),'periodic') && strcmp(bc(2),'periodic'))
    
    n = size(v,1)*2;
    u = zeros(n,1);
    for i = 1:n
        if (i == 1)
            u(i) = 0.5 * (boundary(v,'left',bc)+v(1));
        else
            if (mod(i,2) == 0)
                u(i) = v(i/2);
            else
                j = (i-1)/2;
                u(i) = 0.5 * (v(j)+v(j+1));
            end
        end
    end
    
elseif (strcmp(bc(1),'dirichlet') && strcmp(bc(2),'dirichlet'))
    
    n = 2*size(v,1) - 1;
    u = zeros(n,1);
    u(1) = v(1);
    for i = 2:n-1
        if (mod(i,2) == 1)
            j = (i-1)/2+1;
            u(i) = v(j);
        else
            j = i/2;
            u(i) = 0.5 * (v(j) + v(j+1));
        end
    end
    u(n) = v(size(v,1));
    
end

end

