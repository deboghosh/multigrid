function ub = boundary( u, dir, bctype )

n = size(u,1);
if (strcmp(dir,'left'))
    if (strcmp(bctype(1),'periodic'))
        ub = u(n);
    elseif (strcmp(bctype(1),'dirichlet'))
        ub = 1.0;
    else
        fprintf('Error: bad value for bctype - %s\n',bctype(1));
    end
elseif (strcmp(dir,'right'))
    if (strcmp(bctype(2),'periodic'))
        ub = u(1);
    elseif (strcmp(bctype(2),'dirichlet'))
        ub = 1.0;
    else
        fprintf('Error: bad value for bctype - %s\n',bctype(2));
    end
else
    fprintf('Error: bad value for dir specified - %s\n',dir);
    exit;
end

end

