function [u,n] = solve(A,b,u0,maxits,atol,rtol,stype, verbose, prefix)

u = u0;
for n = 1:maxits
    
    errnorm = twonorm(residual(A,u,b));

    if (n == 1)
        norm0 = errnorm;
    end

    if (errnorm < atol)
        break;
    end
    if (errnorm/norm0 < rtol)
        break;
    end

    if (strcmp(stype,'jacobi'))
        v = jacobi(A,b,u);
    elseif (strcmp(stype,'gauss_seidel'))
        v = gauss_seidel(A,b,u);
    else
        fprintf('Error: bad value for stype - %s.\n',stype);
    end
    u = v;
    
    if ((verbose > 0) && ((mod(n,verbose) == 0) || (n == 1)))
        fprintf('  %s - iter = %5d, norm = %1.4e\n', ...
                prefix, n, twonorm(residual(A,u,b)));

    end
end

if (maxits == 0)
    n = 0;
end

end

