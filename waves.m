% clear;
% close all;
% 
% n = 20;
% for k=0:n
%     i = 0:n-1;
%     f = cos(2*pi*k*(i/n));
%     figure(1);
%     subplot(3,7,k+1);
%     plot(i,f,'-ko');
%     title(sprintf('k=%d',k));
% end

close all;
figidx = 1;

n = 10;
for k=0:2*n
    i = 0:2*n-1;
    f = sin(pi*k*(i/n));
    figure(figidx);
    subplot(3,7,k+1);
    plot(i,f,'-ko');
    title(sprintf('k=%d',k));
end
figidx = figidx + 1;

n = [10,20,40,80,160,320];
maxampfac = zeros(1,size(n,2));
oneminusmaxampfac = zeros(1,size(n,2));
maxampfacr = zeros(1,size(n,2));

for ng=1:size(n,2)
    
    k = 1:n(ng); %1:(2*n(ng)-1);
    theta = pi*k/n(ng);
    ampfac = exp(1i*theta)./(2-exp(1i*theta));
    ampfacmod = abs(ampfac);
    maxampfac(ng) = max(ampfacmod);
    oneminusmaxampfac(ng) = 1 - maxampfac(ng);
    
%     c = 1;
%     kr = (c*n(ng)):(2*n(ng)-1);
    c = 0.5;
    kr = (c*n(ng)):n(ng);
    thetar = pi*kr/n(ng);
    ampfacr = exp(1i*thetar)./(2-exp(1i*thetar));
    ampfacrmod = abs(ampfacr);
    maxampfacr(ng) = max(ampfacrmod);

    
    figure(figidx);
    subplot(2,3,ng);
    plot(k,ampfacmod,'-ko');
    axis([min(k), max(k), 0.3, 1]);
    
end
figidx = figidx + 1;

figure(figidx);
semilogx(n, maxampfac, '-ko');
hold on;
semilogx(n, maxampfacr, '-ro');
figidx = figidx + 1;

figure(figidx);
loglog(n, oneminusmaxampfac, '-ko');
axis tight;
figidx = figidx + 1;

